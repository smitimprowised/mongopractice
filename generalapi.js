const express = require("express");
const bodyParser = require("body-parser");
var cors = require('cors');
const app = express();
var jwt = require('jsonwebtoken');
var configs = require("./config.js");
var dbname = "apimongo";
var bcrypt = require('bcrypt');

const mongoose = require("mongoose");
const url = require("url");
app.use(cors());
app.use(bodyParser.json());

app.use(bodyParser.urlencoded({

	extended : true
}));


mongoose.connect(configs+dbname, {useNewUrlParser:true,useUnifiedTopology: true});

mongoose.set("useCreateIndex",true); 

var userSchema = new mongoose.Schema({ 
 _id: mongoose.Schema.Types.ObjectId,
 user_id : String,
 password : String,
 role_name : String,
 email:String
});

const user = new mongoose.model("User",userSchema);

// Welcome API
app.get("/",(req,res)=>{
  return res.send("Welcome to General API for registration and log in!")
})

// Registering the user 
app.post("/register",(req,res,next)=>{
  user.find({email:req.body.email})
  .exec()
  .then(user =>{
    if(user.length >=1) {
      return res.status(422).json({
        message:'Email exists'
      });
    } else { 
      bcrypt.hash(req.body.password,10,(err,hash)=>{
        if(err) { 
          return res.status(500).json({error:err});
        } else { 
              var newUser = new user({
                email: req.body.email,
                password: hash,
                role_name:req.body.role_name
              });
              newUser
              .save()
              .then(result => {
                console.log(result);
                res.status(200).json({
                  message:'User Registered!!'
                })
              })
              .catch(err => { 
                console.log(err);
                res.status(500).json({
                  error:err
                })
              });
        } 
      })
    }
  })

  
});




app.post("/login",(req,res,next)=>{
  user.find({email:req.body.email})
  .exec()
  .then(user=>{
    if(user.length<1){
      res.status(401).json({
        message:"Auth Falied"
      });
    } 
    bcrypt.compare(req.body.password,user[0].password,(err,result)=>{
      if(err) {
        res.status(401).json({
          message:"Auth Falied"
        });
      }
      if(result) {
       var token= jwt.sign({
          email:user[0].email,
          userId:user[0]._id
        }
        ,'secret'
        ,{
          expiresIn:"1h"
        }
        );
        return res.status(200).json({
          message:'Auth successful',
          token:token
        });
      }
      res.status(401).json({
        message:"Auth Falied"
      });
    });
  })
  .catch(err => { 
    console.log(err);
    res.status(500).json({
      error:err
    })
  })
})

app.listen(4980, () => console.log("Server started on port 4980."));