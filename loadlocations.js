const fs = require("fs");
const mongodb = require("mongodb").MongoClient;
const fastcsv = require("fast-csv");

// let url = "mongodb://localhost:27017/";
var url = require("./config.js")
let stream = fs.createReadStream("location.csv");
let csvData = [];
let csvStream = fastcsv
  .parse()
  .on("data", function(data) {
    csvData.push({
      id: data[0],
      address: data[1],
      city: data[2],
      city_id: data[3],
      country_id: data[4],
      latitude: data[5],
      locality: data[6],
      locality_verbose: data[7],
      longitude: data[8],
      zipcode: data[9]
    });
  })
  .on("end", function() {
    csvData.shift();

    console.log(csvData);

    mongodb.connect(
      url,
      { useNewUrlParser: true, useUnifiedTopology: true },
      (err, client) => {
        if (err) throw err;

        client
          .db("apimongo")
          .collection("location")
          .insertMany(csvData, (err, res) => {
            if (err) throw err;

            console.log(`Inserted: ${res.insertedCount} rows`);
            client.close();
          });
      }
    );
  });

stream.pipe(csvStream);
