const express = require("express");
const bodyParser = require("body-parser");
var bcrypt = require('bcrypt')
var jwt = require('jsonwebtoken')
var checkAuth = require('./check-auth')
var config = require("./config.js");
var dbname = "apimongo"
const app = express();
var cors = require('cors');

app.use(cors());
const mongoose = require("mongoose");
app.use(bodyParser.json())


app.use(bodyParser.urlencoded({

	extended : true
}));

mongoose.connect(config+dbname, {useFindAndModify:false,useNewUrlParser:true,useUnifiedTopology: true});
mongoose.set("useCreateIndex",true); 

var userSchema = new mongoose.Schema({ 
  _id: mongoose.Schema.Types.ObjectId,
  user_id : String,
  password : String,
  role_name : String,
  email:String
 });
 


const roleSchema = {

	role_id : String,
	role_name : String
}

const locationSchema = new mongoose.Schema({

 id: String,
 address : String,
 city : String,
 city_id: String,
 country_id :String,
 latitude :String,
 locality:String,
 locality_verbose:String,
 longitude: String,
 zipcode : String,
 deleted_at : String,
 updated_at : String,
 created_at : String

});

const ratingSchema = new mongoose.Schema({ 

id : String,
aggregate_rating : String,
rating_color:String,
rating_text:String,
votes:String,
deleted_at : String,
updated_at : String,
created_at : String,
email : String
});

const restaurantSchema = new mongoose.Schema({

	id: String,
	average_cost_for_two:String,
	book_url:String,
	cuisines:String,
	currency:String,
	deeplink:String,
	events_url:String,
	featured_image: String,
  has_online_delivery: String,
  has_table_booking: String,
  include_bogo_offers: String,
  is_book_from_web_view: String,
  is_delivering_now: String,
  is_table_reservation_supported: String,
  is_zomato_book_res: String,
  menu_url: String,
  mezzo_provider: String,
  name: String,
  photos_url: String,
  price_range: String,
  thumb: String,
  url: String,
  deleted_at : String,
	updated_at : String,
	created_at : String,

});

const user = new mongoose.model("User",userSchema)
const location = new mongoose.model("Location",locationSchema);

const rating = new mongoose.model("Rating",ratingSchema);

const restaurant = new mongoose.model("Zomato",restaurantSchema);

app.get("/",(req,res)=>{

res.send({message:"Welcome to the Customer API"});

})

app.get("/restaurant/:id",(req,res)=>{

  restaurant.findOne({id:req.params.id})
  .then(rest => { 
      if(!rest) { 
        return res.status(404).send({
          message : "Restaurant Not Found" + req.params.id
        })}
        res.send(rest);

  }).catch(err=>{
    if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "restaurant not found with id " + req.params.noteId
            });                
        }
        return res.status(500).send({
            message: "Error retrieving note with id " + req.params.noteId
        });
  })
})

app.get("/rating/:id",(req,res)=>{

  rating.findOne({id:req.params.id})
  .then(rating => { 
      if(!rating) { 
        return res.status(404).send({
          message : "Restaurant Not Found" + req.params.id
        })}
        res.send(rating);

  }).catch(err=>{
    if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "restaurant not found with id " + req.params.id
            });                
        }
        return res.status(500).send({
            message: "Error retrieving note with id " + req.params.id
        });
  })
})
app.get("/restaurant/table/all",(req,res)=>{
  restaurant.find()
  .then(rest=>{
    res.status(200).json({
      rest:rest
    })
  })
  .catch(err=>{
    return res.status(500).send({
      message: "Error retrieving data " 
  });
  })
})

//1 Get all restaurants list by city with rating ,can be sort by ratings and avg-cost-per-two


app.get("/restaurant/paging",(req,res)=>{

restaurant.aggregate([{
    "$lookup": {
      "from" : "location",
      "localField":"id",
      "foreignField":"id",
      "as":"resultRes"
    }

   },{"$unwind":"$resultRes"} ,
   {
    "$lookup":{
    
      "from" : "ratings",
      "localField":"id",
      "foreignField":"id",
      "as":"resultDet"
   }

   },{"$unwind":"$resultDet"}
   ,
   {
 "$group":{
  "_id": {id:"$resultRes.city"},
  "id": {"$first":"$id"},
  "name":{"$first":"$name"},
  "rating":{"$first":"resultDet.aggregate_rating"},
  "cuisines":{"$first":"$cuisines"},
  "currency":{"$first":"$currency"},
  "average_cost_for_two":{"$first":"$average_cost_for_two"},
  "has_online_delivery":{"$first":"$has_online_delivery"},
  "has_table_booking":{"$first":"$has_table_booking"},
  "is_book_from_web_view":{"$first":"$is_book_from_web_view"},
  "is_zomato_book_res":{"$first":"$is_zomato_book_res"},
  "is_delivering_now":{"$first":"$is_delivering_now"},
  "price_range":{"$first":"$price_range"},
  "rating":{"$first":"$resultDet.aggregate_rating"},
  "latitude":{"$first":"$resultRes.latitude"},
  "longitude":{"$first":"$resultRes.longitude"},
  "locality":{"$first":"$resultRes.locality"},
  "zipcode":{"$first":"$resultRes.zipcode"}
 } }
 

 ]).exec((err,details)=>{
  return res.send(details);
 })


})

//2 get restaurant by id with rating and location details
app.get("/restaurant/rating/location",(req,res)=>{

  restaurant.aggregate([{
    "$lookup": {
      "from" : "location",
      "localField":"id",
      "foreignField":"id",
      "as":"resultRes"
    }

   },{"$unwind":"$resultRes"} ,
   {
    "$lookup":{
    
      "from" : "ratings",
      "localField":"id",
      "foreignField":"id",
      "as":"resultDet"
   }

   },{"$unwind":"$resultDet"}
   ,
   {
 "$group":{
  "_id": "$id",
  "id": {"$first":"$id"},
  "name":{"$first":"$name"},
  "cuisines":{"$first":"$cuisines"},
  "currency":{"$first":"$currency"},
  "has_online_delivery":{"$first":"$has_online_delivery"},
  "has_table_booking":{"$first":"$has_table_booking"},
  "is_book_from_web_view":{"$first":"$is_book_from_web_view"},
  "is_zomato_book_res":{"$first":"$is_zomato_book_res"},
  "is_delivering_now":{"$first":"$is_delivering_now"},
  "price_range":{"$first":"$price_range"},
  "rating":{"$first":"$resultDet.aggregate_rating"},
  "latitude":{"$first":"$resultRes.latitude"},
  "longitude":{"$first":"$resultRes.longitude"},
  "locality":{"$first":"$resultRes.locality"},
  "zipcode":{"$first":"$resultRes.zipcode"}
 } 
  
 }]).exec((err,details)=>{
  return res.send(details);
 })

})

// 3 get restaurant count by city's locality and by cuisines
app.get("/restaurant/number/count",(req,res)=>{

  restaurant.aggregate([{
    "$lookup": {
      "from" : "location",
      "localField":"id",
      "foreignField":"id",
      "as":"resultRes"
    }

   },{"$unwind":"$resultRes"} ,{

      "$group" : {
        "_id": {locality:"$joinArray.locality",cuisines:"$cuisines"},
        "count": {"$sum":1}
      }
   }]).exec((err,rest)=>{
    return res.send(rest)
   })
  
})

// 4 Get restaurants list by locality and cuisines 
app.get("/restaurant/loca/list",(req,res)=>{

  restaurant.aggregate([{
    "$lookup": {
      "from" : "location",
      "localField":"id",
      "foreignField":"id",
      "as":"resultRes"
    }

   },{"$unwind":"$resultRes"},{

      "$group" : {
        "_id": {locality:"$joinArray.locality",cuisines:"$cuisines"},
        "name":{$push:"$name"},
        //"count": {"$sum":1}
      }
   }]).exec((err,rest)=>{
    return res.send(rest)
   })
  
})

//5 Add,Update,delete ratings

app.post("/login",(req,res,next)=>{
  user.find({email:req.body.email})
  .exec()
  .then(user=>{
    if(user.length<1){
      res.status(401).json({
        message:"Auth Falied"
      });
    } 
    bcrypt.compare(req.body.password,user[0].password,(err,result)=>{
      if(err) {
        return res.status(401).json({
          message:"Auth Falied "+ user[0].password
        });
      }
      if(result) {
       var token= jwt.sign({
          email:user[0].email,
          userId:user[0]._id
        }
        ,'secret'
        ,{
          expiresIn:"1h"
        }
        );
        return res.status(200).json({
          message:'Auth successful',
          token:token
        });
      }
      res.status(401).json({
        message:"Auth Falied"
      });
    });
  })
  .catch(err => { 
    console.log(err);
    res.status(500).json({
      error:err
    })
  })
})

app.get("/loggedIn/user",checkAuth,(req,res)=>{

  res.send("Hello "+req.userData.email);
})


app.get("/restaurant/total",(req,res)=>{
  restaurant.count({},(err,result)=>{
    if(err) { 
      return res.json({message:"Error"})
    } else { 

       return res.json({message: result});
    }
  })
})

app.get("/rating/record/total",(req,res)=>{
  rating.count({},(err,result)=>{
  if(err) {
  return res.json({message:"Error"})
  } else {
  
  return res.json({message: result});
  }
  })
  })
app.get("/rating/data/all",(req,res)=>{
    rating.find()
    .then(rating=> {
      res.status(200).json({
        rating:rating
      })
    })
    .catch(err=>{
      return res.status(500).send({
        message: "Error retrieving data " 
    }); })
})
app.post("/rating/record/add",checkAuth,(req,res)=>{

    var d = new Date()
    const newRating = new rating({
    id: req.body.id,
    aggregate_rating: req.body.aggregate_rating,
    rating_color :req.body.rating_color,
    rating_text : req.body.rating_text,
    votes : req.body.votes,
    created_at: d,
    email : req.userData.email
   })

  newRating.save((err,rating)=>{
   
    if(err) {  return res.send(err)}
    else { return res.json({message:"Rating Added Successfully!",rating})}
  
  })
})


app.delete("/rating/record/delete",checkAuth,(req,res)=>{

  var d = new Date();
 rating.findOneAndRemove({email:req.userData.email,id:req.body.id},{new:true},(err,rating)=>{
    if(err) return res.send(err);
    return res.json({message:"Rating Deleted Successfully!!",rating});
  })
})

app.put("/rating/record/update",checkAuth,(req,res)=>{

var d = new Date()
rating.findOneAndUpdate({email:req.userData.email,id:req.body.id},{$set:{aggregate_rating:req.body.rating,updated_at:d,rating_color:req.body.rating_color,rating_text:req.body.rating_text}},
  {returnOriginal:false},(err,rating)=>{
    if(err) return res.send(err);
    return res.json({message:"Rating Updated Successfully!!",rating});
  })
})


//6 Search Restaurant (by name and cuisines)
app.get("/restaurant/data/summary/",(req,res)=>{

var restaurantName = req.params.name;
var cuisinesName  = req.params.cuisines;

 restaurant.find({$and:[{name:req.body.name},{cuisines:req.body.cuisines}]},(err,restaurants)=>{
  if(err) { return res.send(err)}
  else { res.send(restaurants)}
})
})

if(process.env.NODE_ENV !=='test'){
app.listen(5556, () => console.log("Server started on port 5556."));
}

module.exports = app;