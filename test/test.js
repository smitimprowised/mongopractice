
var assert = require("assert");
var request = require("supertest");
var chai = require("chai");
var chaiHttp = require("chai-http");
var should = chai.should();
var expect = require("chai").expect;
var util = require("util");
const express = require("express");

process.env.NODE_ENV = "test"

var server = require("../customerapi.js");

chai.use(chaiHttp);

describe("Get all restaurants list by city with rating", ()=> {

	it("it should get restaurant list grouped by city and rating",(done)=>{

	chai.request(server)
		.get("/restaurant/paging")
		.end((err,res)=>{
			res.should.have.status(200);
			done();
   			})

   })

})

describe("/GET rating location",()=>{

   it("it should get rating and location details by id",(done)=>{

   		chai.request(server)
   			.get("/restaurant/rating/location")
   			.end((err,res)=>{
   				res.should.have.status(200);
   				done();
   			})

   })

})


describe("/GET res count by city's locality and by cuisines",()=>{

   it("it should get count group by locality and cuisines",(done)=>{

   		chai.request(server)
   			.get("/restaurant/count")
   			.end((err,res)=>{
   				res.should.have.status(200);
   				done();
   			})

   })

})

describe("/GET rest list by locality and cuisines ",()=>{

   it("it should get restaurant list group by locality and cuisines",(done)=>{

   		chai.request(server)
   			.get("/restaurant/list")
   			.end((err,res)=>{
   				res.should.have.status(200);
   				done();
   			})

   })

})

describe("/GET search by restaurant name and cuisines ",()=>{

   it("it should get restaurant summery group by name and cuisines",(done)=>{

   		chai.request(server)
   			.get("/restaurant/summary")
   			.end((err,res)=>{
   				res.should.have.status(200);
   				done();
   			})

   })

})

describe("/POST add rating row to the collection ",()=>{

   it("it should add rating column to the collection",(done)=>{

   	var d = new Date()
	var rating = {
   	   	id: 6318213,
    	aggregate_rating: 4.99,
    	rating_color :"5BA829",
    	rating_text : "Excellent",
    	votes : 600,
    	created_at:d,
   }

   		chai.request(server)
   			.post("/rating/add")
   			.send(rating)
   			.end((err,res)=>{
   				res.should.have.status(200);
   				res.body.should.have.property('message').eql("Rating Added Successfully!");
   				res.body.rating.should.have.property("id");
   				res.body.rating.should.have.property("aggregate_rating");
   				res.body.rating.should.have.property("rating_color");
   				res.body.rating.should.have.property("rating_text");
   				res.body.rating.should.have.property("votes");
   				res.body.rating.should.have.property("created_at");
   				done();
   			})

   })

})

describe("/PUT update rating details to the collection", ()=>{

	it("it should update rating fields given user_id",(done)=>{
		var d = new Date();
		var rating = {id:6316125,aggregate_rating:4.9};

		chai.request(server)
			.put("/rating/update" + rating.id)
			.send({aggregate_rating:4.9})
			.end((err,res)=>{

				res.should.have.status(200);
				res.body.rating.should.have.property("aggregate_rating").eql(4.9);
				done();
			})

	})
})

describe("/DELETE delete rating details to the collection", ()=>{

	it("it should delete rating rows given user_id",(done)=>{
		var rating = {user_id:2};
		// rating.save((err,rating)=>{

		chai.request(server)
			.delete("/rating/delete" + rating.user_id)
			.end((err,res)=>{

				res.should.have.status(200);
				res.body.rating.should.have.property("aggregate_rating").eql(4.9);
				done();
			})

	})
})




