const express = require("express");
const bodyParser = require("body-parser");
var jwt = require('jsonwebtoken');
var bcrypt = require('bcrypt');
var url = require("./config.js");
var dbname = "apimongo";
const app = express();
var cors = require('cors');
const mongoose = require("mongoose")
app.use(bodyParser.json())
mongoose.set('useFindAndModify', false);

app.use(cors());
app.use(bodyParser.urlencoded({

	extended : true
}));

mongoose.connect(url+dbname, {useNewUrlParser:true,useUnifiedTopology: true});

var userSchema = new mongoose.Schema({ 
  // _id: mongoose.Schema.Types.ObjectId,
  user_id : String,
  password : String,
  role_name : String,
  email:String
 })
 

const roleSchema = {

	role_id : String,
	role_name : String
}

const locationSchema = {

 id: String,
 address : String,
 city : String,
 city_id: String,
 country_id :String,
 latitude :String,
 locality:String,
 locality_verbose:String,
 longitude: String,
 zipcode : String,
 deleted_at : String,
 updated_at : String,
 created_at : String

}

const ratingSchema = { 

id : String,
aggregate_rating : String,
rating_color:String,
rating_text:String,
votes:String,
deleted_at : String,
updated_at : String,
created_at : String,
email: String
}

const restaurantSchema = {

	id: String,
	average_cost_for_two:String,
	book_url:String,
	cuisines:String,
	currency:String,
	deeplink:String,
	events_url:String,
	featured_image: String,
  has_online_delivery: String,
  has_table_booking: String,
  include_bogo_offers: String,
  is_book_from_web_view: String,
  is_delivering_now: String,
  is_table_reservation_supported: String,
  is_zomato_book_res: String,
  menu_url: String,
  mezzo_provider: String,
  name: String,
  photos_url: String,
  price_range: String,
  thumb: String,
  url: String,
  deleted_at : String,
	updated_at : String,
	created_at : String,

}

const user = new mongoose.model("User",userSchema);

const role = new mongoose.model("Role",roleSchema);

const location = new mongoose.model("Location",locationSchema);

const rating = new mongoose.model("Rating",ratingSchema);

const restaurant = new mongoose.model("Zomato",restaurantSchema);


app.get("/",(req,res)=>{

	res.json({message:"Welcome the Admin API"});
});


app.post("/restaurant/add",(req,res)=>{

  var d = new Date()
  var role = req.body.role;
  if(role==="Admin"){

const newRestaurant = new restaurant({
    id: req.body.id,
    average_cost_for_two:req.body.average_cost_for_two,
    book_url: req.book_url,
    cuisines: req.body.cuisines,
    currency: req.body.currency,
    deeplink: req.body.deeplink,
    events_url: req.body.events_url,
    featured_image: req.body.featured_image,
    has_online_delivery: req.body.has_online_delivery,
    has_table_booking: req.body.has_table_booking,
    include_bogo_offers: req.body.include_bogo_offers,
    is_book_from_web_view: req.body.is_book_from_web_view,
    is_delivering_now: req.body.is_delivering_now,
    is_table_reservation_supported: req.body.is_table_reservation_supported,
    is_zomato_book_res: req.body.is_zomato_book_res,
    menu_url: req.body.menu_url,
    mezzo_provider: req.body.mezzo_provider,
    name: req.body.name,
    photos_url: req.body.photos_url,
    price_range: req.body.price_range,
    thumb: req.body.thumb,
    url: req.body.url,
    created_at : d
   })

  newRestaurant.save((err,rest)=>{
   
    if(err) {  return res.send(err)}
    else { return res.json({message:"Restaurant Added Successfully!",rest})}
  })
} else { res.json({message:"Non-Admin user are not be able to add!"})}

})

app.get("/restaurant/total",(req,res)=>{
  
  restaurant.count({},(err,result)=>{
    if(err) { 
      return res.json({message:"Error"})
    } else { 

       return res.json({message: result});
    }
  })
})

app.put("/restaurant/update",(req,res)=>{

  const role = req.body.role;
  var datetime = new Date();

  if (role === "Admin") {

  restaurant.findOneAndUpdate({id:req.body.id},{$set:{price_range:req.body.price_range,average_cost_for_two:req.body.average_cost_for_two,cuisines:req.body.cuisines,currency:req.body.currency,name:req.body.name,updated_at:datetime}},{"new":true},(err,rest)=>{

  if(err) { res.send(err)};
  res.send({message:"restaurant details has been updated in restaurant collection Successfully!",rest})
  })
  
  } else { res.json({message:"Non-Admin user are not be able to add!"})}

})

app.use("/restaurant/delete",(req,res)=>{

 const role = req.body.role;
  var datetime = new Date();

  if (role === "Admin") {

  restaurant.findOneAndRemove({id:req.body.id},{"new":true},(err,rest)=>{

  if(err) { res.send(err)};
  res.send({message:"restaurant has been deleted in restaurant collection Successfully!!",rest})
  })
  
  }

})

app.get("/location/record/total",(req,res)=>{
  
  location.count({},(err,result)=>{
    if(err) { 
      return res.json({message:"Error"})
    } else { 

       return res.json({message: result});
    }
  })
})

app.get("/location/data/all",(req,res)=>{
  location.find()
  .then(rating=> {
    res.status(200).json({
      rating:rating
    })
  })
  .catch(err=>{
    return res.status(500).send({
      message: "Error retrieving data " 
  }); })
})

app.get("/location/:id",(req,res)=>{

  location.findOne({id:req.params.id})
  .then(location => { 
      if(!location) { 
        return res.status(404).send({
          message : "Location Not Found" + req.params.id
        })}
        res.send(location);

  }).catch(err=>{
    if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Location not found with id " + req.params.id
            });                
        }
        return res.status(500).send({
            message: "Error retrieving data with id " + req.params.id
        });
  })
})

app.post("/location/record/add",(req,res)=>{

  var d = new Date()
  var role = req.body.role;

  if(role==="Admin"){

const newLocation = new location({
    id: req.body.id,
    address : req.body.address,
    city : req.body.city,
    city_id: req.body.city_id,
    country_id :req.body.country_id,
    latitude :req.body.latitude,
    locality:req.body.locality,
    locality_verbose:req.body.locality_verbose,
    longitude: req.body.longitude,
    zipcode : req.body.zipcode,
    created_at : d
   })

  newLocation.save((err,loc)=>{
   
    if(err) {  return res.send(err)}
    else { return res.json({message:"Location Added Successfully!",loc}) }
  })
} else { res.json({message:"Non-Admin user are not be able to add!"})}

})



app.delete("/location/record/delete",(req,res)=>{

	const role = req.body.role;
	var d = new Date();

	if (role === "Admin") {

  location.findOneAndRemove({id:req.body.id},{"new": true},(err,loc)=>{

        res.send({message:"Location deleted Successfully!!",loc})

  })
	} else {res.send({message:"Non-Admin are not allowed to delete locations collection!!"})}

})

app.put("/location/record/update",(req,res)=>{

	const role = req.body.role;
	var datetime = new Date();


	if (role === "Admin") {
        
    location.findOneAndUpdate({id:req.body.id},{$set:{city:req.body.city,locality:req.body.city,updated_at:datetime}},(err,loc)=>{

      if(err){return res.send(err)}
      else { return res.json({message:"Successfully updated the city feild in location record!",loc}) }

    })
  } else {return res.send("Non-Admin user can not update User's password!!") }

})

// user api 

app.get("/user/data/all",(req,res)=>{
  user.find()
  .then(user=> {
    res.status(200).json({
      user:user
    })
  })
  .catch(err=>{
    return res.status(500).send({
      message: "Error retrieving data " 
  }); })
})

app.get("/user/:email",(req,res)=>{

  user.findOne({email:req.params.email})
  .then(use => { 
      if(!use) { 
        return res.status(404).send({
          message : "User Not Found" + req.params.email
        })}
        res.send(use);

  }).catch(err=>{
    if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "User not found with email " + req.params.email
            });                
        }
        return res.status(500).send({
            message: "Error retrieving data with email" + req.params.email
        });
  })
})

app.post("/user/record/add",(req,res,next)=>{
  user.find({email:req.body.email})
  .exec()
  .then(use =>{
    if(use.length >=1) {
      return res.status(422).json({
        message:'Email exists'
      });
    } else { 
      bcrypt.hash(req.body.password,10,(err,hash)=>{
        if(err) { 
          return res.status(500).json({error:err});
        } else { 
              const newUser = new user({
                email: req.body.email,
                password: hash,
                role_name:req.body.role_name
              })

              newUser
              .save()
              .then(result => {
                console.log(result);
                res.status(200).json({
                  message:'User Registered!!'
                })
              })
              .catch(err => { 
                console.log(err);
                res.status(500).json({
                  error:err
                })
              });
        } 
      })
    }
  })
});



app.delete("/user/record/delete",(req,res,next)=>{

  if (req.body.role === "Admin") {

    user.findOneAndRemove({email:req.body.email}, (err,user)=>{

      if(err){ return next(err);}
          else { return res.json({message:"Successfully deleted the user!!",user})}

    })
  }

})

app.put("/user/record/update",(req,res)=>{
  
  if(req.body.role==="Admin"){

    user.findOneAndUpdate({email:req.body.email}, {$set:{pwd:req.body.password}}, (err,user)=>{

      if(err){ return next(err);}
          else { return res.json({message:"Successfully updated the user password!!",user})}

    })

} 

else {return res.send("Non-Admin user can not update User's password!!") }

})

if(process.env.NODE_ENV !=='test'){
app.listen(4930, () => console.log("Server started on port 4930."));
}

module.exports = app;  