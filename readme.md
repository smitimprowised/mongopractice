# Rest APIs Practice
## Starting Project
 - npm init (to start the new package)
 - npm install --save body-parser chai chai-http express express-handlebars express-paginate fast-csv jsonwebtoken mocha mongoose request should supertest swagger-ui-express sync-request (to install all the necessary packages, will get all packeges in package.json file)
 - nodemon <filename> to start the apis (i.e nodemon customerapi.js) , after that can use APIs
## Loading CSV file into collections
- Loading location.csv file using loadlocation.js ( Run loadlocations.js using "node loadlocations.js" will load all the records to database: "apimongo" and collection: "location")
- Loading rating.csv file using loadrating.js ( Run loadrating.js using "node loadrating.js" will load all the records to database: "apimongo" and collection: "ratings")
- Loading zomato.csv file using loadzomtatotocsv.js ( Run loadzomtatotocsv.jsusing "node loadzomtatotocsv.js" will load all the records to database: "apimongo" and collection: "zomatos")

> Test all the below APIs with POSTMAN utility 
> Pass all required fields to add , update or delete though Body (within that option called x-www-form-urlencoded).  

## Admin API
### Restaurant
- Adding restaurant details  (http://localhost:4930/restaurant/add)
- Updating restaurant details (http://localhost:4930/restaurant/update)
- Delete Restaurant detail (http://localhost:4930/restaurant/update)
### Location
- Adding location details of the restaurant  (http://localhost:4930/location/add)
- Updating location details of the restaurant (http://localhost:4930/location/update)
- Deleting location details of the restaurant  (http://localhost:4930/location/delete)
### User
- Adding new user  (http://localhost:4930/user/add)
- Updating the user password (http://localhost:4930/user/update)
- Deleting User from collection.  (http://localhost:4930/user/delete)


## Customer API
- Displaying all restaurants list by city using Aggregate (http://localhost:5555/restaurant/paging)
- Dispalying restaurant details by id with rating and location details. (http://localhost:5555/restaurant/rating/location)
- Print count of restaurants by city's locality and by cuisines using aggregate(SQL join) (http://localhost:5555/restaurant/count)
- Printing Restaurants list by locality and by cuisines using group by and aggregate( SQL join and groupby) (http://localhost:5555/restaurant/list).
- Adding, Updatating, deleting their own Ratings details( from ratings collections) to perticular restaurant.
		(http://localhost:5555/rating/add)
		(http://localhost:5555/rating/update)
		(http://localhost:5555/rating/delete)
- Displaying Restaurant summary (group by name, by cusisines). (http://localhost:5555/restaurant/summary)


## General API
### User Registration
- Registering the user (api_key,pwd,role_name) (http://localhost:4980/register)
- Login ( user login using passport)  (http://localhost:4980/login)

