const fs = require("fs");
const mongodb = require("mongodb").MongoClient;
const fastcsv = require("fast-csv");

//let url = "mongodb://localhost:27017/";
var url = require("./config.js")
let stream = fs.createReadStream("zomato.csv");
let csvData = [];
let csvStream = fastcsv
  .parse()
  .on("data", function(data) {
    csvData.push({
      id: data[0],
      average_cost_for_two: data[1],
      book_url: data[2],
      cuisines: data[3],
      currency: data[4],
      deeplink: data[5],
      events_url: data[6],
      featured_image: data[7],
      has_online_delivery: data[8],
      has_table_booking: data[9],
      include_bogo_offers: data[10],
      is_book_from_web_view: data[11],
      is_delivering_now: data[12],
      is_table_reservation_supported: data[13],
      is_zomato_book_res: data[14],
      menu_url: data[15],
      mezzo_provider: data[16],
      name: data[17],
      photos_url: data[18],
      price_range: data[19],
      thumb: data[20],
      url: data[21]
    });
  })
  .on("end", function() {
    csvData.shift();

    console.log(csvData);

    mongodb.connect(
      url,
      { useNewUrlParser: true, useUnifiedTopology: true },
      (err, client) => {
        if (err) throw err;

        client
          .db("apimongo")
          .collection("zomato")
          .insertMany(csvData, (err, res) => {
            if (err) throw err;

            console.log(`Inserted: ${res.insertedCount} rows`);
            client.close();
          });
      }
    );
  });

stream.pipe(csvStream);
